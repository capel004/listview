﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static listview.MainPage;

namespace listview
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MorePersonInfo : ContentPage
	{
		public MorePersonInfo ()
		{
			InitializeComponent ();
		}
        public MorePersonInfo(Person person)
        {
            InitializeComponent();

            BindingContext = person;
        }
    }
}