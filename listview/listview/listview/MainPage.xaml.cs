﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace listview
{
    public partial class MainPage : ContentPage
    {
        public class Person
        {
            public
                string FirstName
            {
                get;
                set;
            }
            public string LastName
            {
                get;
                set;
            }
            public string url
            {
                get;
                set;
            }
            public ImageSource Image_icon
            {
                get;
                set;
            }
        }
        public MainPage()
        {
            InitializeComponent();
            Populatelistview();
        }

        private void Populatelistview()
        {
            var Listofpeople = new ObservableCollection<Person>()
            {
                new Person()
                { FirstName="Daenerys" , LastName = "Targaryen",
                url = "http://gameofthrones.wikia.com/wiki/Daenerys_Targaryen",
                Image_icon=ImageSource.FromFile("Daenery.jpg") ,
                },
                new Person()
                { FirstName="Joffrey" , LastName = "Baratheon",
                url = "http://gameofthrones.wikia.com/wiki/Joffrey_Baratheon",
                Image_icon=ImageSource.FromFile("Joffrey.jpg"),
                },
                 new Person()
                { FirstName="Bran " , LastName = "Stark",
                url = "http://gameofthrones.wikia.com/wiki/Bran_Stark",
                Image_icon=ImageSource.FromFile("Bran.png"),
                },
                  new Person()
                { FirstName="Jon " , LastName = "Snow",
                url = "http://gameofthrones.wikia.com/wiki/Jon_Snow",
                Image_icon=ImageSource.FromFile("Jon.png"),
                },
                   new Person()
                { FirstName="Tyrion " , LastName = "Lannister",
                url = "http://gameofthrones.wikia.com/wiki/Tyrion_Lannister",
                Image_icon=ImageSource.FromFile("Tyrion.jpg"),
                }

            };

            list_name.ItemsSource = Listofpeople;

        }

        void HandleMoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var person = (Person)menuItem.CommandParameter;
            Navigation.PushAsync(new MorePersonInfo(person));
        }
        void HandleDeleteClicked(object sender, System.EventArgs e)
        {
            //var menuItem = (MenuItem)sender;
           // var person = (Person)menuItem.CommandParameter;
            //list_name.Remove (person);
        }
    }
        


}

 